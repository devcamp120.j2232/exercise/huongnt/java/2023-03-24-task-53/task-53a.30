public class App {
    public static void main(String[] args) throws Exception {
        // khởi tạo đối tượng employee1 có tham số
        Employee employee1 = new Employee(1, "Huong", "Nguyen", 10000000);
        Employee employee2 = new Employee(2, "Thanh", "Tran", 20000000);

        System.out.println("Employee 1");
        System.out.println(employee1.toString());
        System.out.println(employee1.getAnnualSalary());
        System.out.println(employee1.raiseSalary(10));

        System.out.println("Employee 2");
        System.out.println(employee2.toString());
        System.out.println(employee2.getAnnualSalary());
        System.out.println(employee2.raiseSalary(20));
    }
}
