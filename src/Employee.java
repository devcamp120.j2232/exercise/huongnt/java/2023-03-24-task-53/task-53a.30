public class Employee {
    // khai báo thuộc tính
    public int id;
    public String firstName;
    public String lastName;
    public int salary;

    // phương thức khởi tạo
    public Employee(int id, String firstName, String lastName, int salary) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.salary = salary;
    }

    // getter
    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getName() {
        return (firstName + lastName);
    }

    public int getSalary() {
        return salary;
    }

    public int getAnnualSalary() {
        return salary * 12;
    }

    public int raiseSalary(int percent) {
        return ((salary*percent)/100) + salary;
    }

    // Setter

    public void setSalary(int salary) {
        this.salary = salary;
    }

        //in ra console log
        @Override
        public String toString(){
            return String.format("Employee [id = %s, firstname = %s, lastname = %s, salary = %s]", id, firstName, lastName, salary);
        }
    

}
